<h1>Jogos Digitais: Final Project</h1>

<h2> Carlos Ramoa, Marco Primo, Pedro Ferreirinha, Gustavo Delerue</h2>

<h2>January 31, 202 3</h2>


<h2> Contents</h2>

- 1 Introduction
- 2 General Vision
   - 2.1 Concept
   - 2.2 Characteristics
   - 2.3 Genre
   - 2.4 Target Public
- 3 Story
- 4 Main Character - Our Hero
- 5 Game Interface
   - 5.1 Game Controls
   - 5.2 Game Mechanics
   - 5.3 Scenes
   - 5.4 Win/Defeat
   - 5.5 Difficulty
   - 5.6 HUD
   - 5.7 Camera
- 6 Game World


<h2> 1 Introduction</h2>

For this final presentation of this class, we were asked to create our own game concept, design it and
implement the prototype of (at least) one game level. In this report, we will talk about our vision for
our game, its story, and the mechanics behind it.

<h3> 2 General Vision </h3>

<h4> 2.1 Concept </h4>

INDIVIDUATIO is a thrilling 3D adventure game that takes you on a journey through the innermost
emotions of the human mind. The player takes on the role of the hero, navigating through a surreal
nightmare world filled with enemies representing different feelings such as anger, anxiety, and depression. 
The goal of the game is to collect the three masks that symbolize overcoming these emotions.
As the hero travels deeper into the nightmare, they must face increasingly challenging obstacles and
enemies, using the masks they have collected to defeat them. The environment and atmosphere of the
game become increasingly intense, testing the hero’s resolve and strength of mind. INDIVIDUATIO
is a thought-provoking and imaginative game that challenges players to confront their own emotions
and fears, in a journey towards self-discovery and growth.



![alt text](https://lh6.googleusercontent.com/cdb_8MmyAhzXY6b41uqIrl_uMJKxM41TCekfh5RHXIKq5v2bqnAlJ3cmlMmQuypGtlQ_JTk00RdOusaP2hhnIsVHzDZPU9bUTSlyEul5Z_l6kgrbUl7DmVeSiKltm-YtlojJevChf5VPA2-qeIqM-NSPmg=s2048)



<h4> 2.2 Characteristics</h4>

Players navigate through surreal environments, facing off against enemies representing negative emo-
tions, with the ability to unlock new abilities and powers as they collect the masks.

<h4> 2.3 Genre</h4>

Action-Adventure, Psychological Thriller.

<h4> 2.4 Target Public</h4>

Young adults, gamers seeking a more thought-provoking and introspective experience.


<h3> 3 Story </h3>

INDIVIDUATIO is an action-packed adventure game that takes you on a journey through the hero’s
nightmare. The protagonist is a man who has been trapped in a world filled with monsters and
obstacles. He must overcome his fears and collect the three masks that represent different emotions:
anger, anxiety, and depression. As the hero navigates through this dark world, he will encounter
various enemies that are colored to match the masks. To defeat these enemies, the hero must be
wearing a mask with the same color. The journey is not just about defeating enemies, though. The
hero must also face his inner demons and confront the root causes of his emotions. As the hero collects
each mask, he will begin to feel a sense of liberation and clarity. He will start to see things in a new
light and gain new perspectives on life. With each mask he collects, the hero will become stronger and
more capable of overcoming the challenges that lie ahead. In the end, the hero must face the source of
his nightmare and defeat it once and for all. Only then will he be able to wake up from his nightmare
and live a life free from the shackles of his emotions. Join the hero on his journey and help him unlock
the secrets of INDIVIDUATIO.
The game follows the story of our main protagonist, a man who, at a given point if his life, lost
his true identity. In the beginning, we (players) are hinted that our protagonist is in a coma state.
Inside his mind we play in a big forest type of maze, that seems to have no ending. Without much
information given, as we evolve and advance through the huge forest, we understand that our main
objective is to find broken pieces of a mask that are scattered around the forest. With the puzzle of
the mask completed, our hero can finally understand what happened and get a hold of himself and his
life. Of course this won’t be an easy task as we are faced with several challenges all involving feelings
and experiences that our protagonist has felt.
Each piece of the mask has a certain colour that is linked to a feeling. We may think that colours
are linked to good feelings and memories but in this story we explore the other side of things. As we
are going to see in this game, every colour has two sides (as in life). For example: the colour Red
relates to rage, violence and aggressive behavior. The colour blue is linked with depression, sadness
and loneliness that our hero has faced. Green is our final colour, we may think its linked with good luck
and charm, but actually, green may also be perceived negatively when associated with materialism,
envy, greed and possessiveness.
After gathering all the pieces of the mask, the player has to defeat the final boss, in order for him
to wake up from the coma, and try to finally understand what made him fall in this state.


<h3> 4 Main Character - Our Hero </h3>

The hero in INDIVIDUATIO is a guy who finds himself trapped in a nightmare, struggling to escape.
He is brave and determined, with a strong will to overcome the obstacles in his path. He is constantly
searching for a way out, but with each step he takes, the nightmare only becomes more vivid and
intense. As he journeys deeper into the nightmare, the hero begins to realize that the only way to
escape is to confront the emotions that are holding him captive. He must collect the three masks
of anger, anxiety, and depression and defeat the enemies that embody these emotions. The hero
is athletic, nimble, and quick on his feet, able to dodge enemy attacks and navigate the treacherous
terrain of the nightmare world. He is also a master of strategy, using his wits to outsmart his opponents
and overcome the challenges that lie ahead. Despite the fear and uncertainty that surrounds him, the
hero remains steadfast in his determination to break free from the nightmare. He is a symbol of hope,
a shining example of what it means to be truly brave and face one’s fears head-on.

<p align="center">
   <img src="https://lh6.googleusercontent.com/aQ1Lj-NSl6i_kr7lf89UC6CO4DuEI5guriWpsvRyuqY1lWTgUYlNCqWG3xQNmEPk9qecOhRemAV9nwvGDDkSH5xwekaUyZb7p5n6ltd3jDBlZtmKmwfLgT1CsltckR7jJ4u4Odn520UTtJFVJ7QuSRVD6g=s2048"/>

</p>




<h3> 5 Game Interface</h3>

<h4>  5.1 Game Controls </h4>

The game supports a broad of interfaces since in the development the new Unity Input System was used. However, to keep it simple, this section only describes the commands interface of keyboard and mouse with the following list of commands:


```
W-A-S-D (directional arrows) - player movement
Space - Jump
F - Talk to NPCs
Right mouse click - Basic attack
Left Shift - Dash + Sword swing
X - Dash
```
<h4>  5.2 Game mechanics </h4> 

The mechanics of INDIVIDUATIO involve collecting three masks that represent different emotions:
anger, anxiety, and depression. Each mask has a corresponding color and can only be used to defeat
enemies with the same color. The hero can use his sword to defeat enemies, but using the masks makes
it easier. The hero also has two abilities, which can be used to defeat enemies and solve puzzles. The
game is set in a dream-like environment and progress through the game involves overcoming various
challenges, including solving puzzles and defeating enemies. The objective of the game is to collect all
three masks and escape the nightmare by waking up from the dream.



<p align="center" width="100%">
    <img  width="30%" src="https://lh5.googleusercontent.com/PattuJuto_C0DElAak5A00kdUS5oaLkNrK-orPppMX1NIBxyNOmy16baWOHKsRWuALOAoIomnINEsLCeOOpgJvkPdIR4sQgloRCmfPJ2itQh5AQ8ud2Js_5u36KH-u7p3ptaUwwgzP2UlYducSfM5ddYeQ=s2048
">
</p>


<p align="center" width="100%">
    <img  width="30%" src="https://lh3.googleusercontent.com/IIdJcmwDkP17VC-IJ4PWg2jEuCeKxiG0n31CRGLAh2zZtya8dahURuKFPYQbio1_X_H1h54wsgEYUSe4a1BKlORpQ-Z54EX0fubq_9SbszHgRpkdQFbOmEx9dWzOAICWSss_xF7jFPeptGLppwz4ckRtMA=s2048
">
</p>


<h4>  5.3 Scenes</h4> 

The environment in INDIVIDUATIO is a dreamlike, surreal world that shifts and evolves as the player
progresses through the game. It is composed of four sections, three of which are illuminated with
the same color as one of the three masks that the player must collect. These colored sections are
where the masks float, waiting to be picked up by the player. However, once the player obtains a
mask, enemies that match the color of the mask will appear, forcing the player to engage in combat.
The fourth section is the initial area where, once all three masks have been collected, a dragon will
spawn, representing the final challenge that the player must overcome to escape the nightmare. The
environment is atmospheric and eerie, with low lighting and eerie sounds to create an unsettling and
immersive experience for the player.


<p align="center" width="100%">
    <img  width="100%" src="https://lh5.googleusercontent.com/leDLOFWLqOgNI95yo9XBKEQIc5KzuE-5_8gqFSFtEsfgey4xnO-Gq14iCeuNBcgbvVVwhoQbPaDKrhAVTQdCPRqDfr3pQxljAQ1918b5qUA2WrorUwhbDjfd-_DvnqTRe5whixh-bCJP3bM5q6xIcX4NXw=s2048
">
</p>




<h4>  5.4 Win/Defeat</h4> 

The victory condition of INDIVIDUATIO is for the player to collect all 3 masks and defeat the dragon
in the central area. The player must overcome their emotions represented by the masks to defeat the
enemies and reach the final challenge. By defeating the dragon, the player will have conquered their
inner demons and will have achieved self-discovery and growth. The game ends with the hero waking
up from the nightmare, symbolizing their triumph over their emotional struggles. The defeat condition
in INDIVIDUATIO is when the player’s life reaches 0. If the player is hit by an enemy and loses all
5 life points, the game will end and the player will have to start over from the beginning. The player
must collect all 3 masks, defeat all enemies and overcome the dragon to reach victory and escape the
nightmare.

<h4>  5.5 Difficulty</h4> 

In INDIVIDUATIO, the difficulty level can be adjusted according to the player’s preference. The
player can choose to play on easy, normal or hard mode. On easy mode, enemies will have less health
and deal less damage, allowing the player to progress through the game more quickly. On normal
mode, the enemies will be balanced, giving the player a moderate challenge. On hard mode, enemies
will be much more difficult to defeat, and the player will need to use all of their skills and abilities to
survive. The difficulty level can be changed at any time during the game, so players can adjust the
challenge to their liking.



<p align="center" width="100%">
    <img  width="100%" src="https://lh5.googleusercontent.com/dqCFiv8diIpbKJFd6dreb0pbiTaqfLiR8G5S1rb1CEwx5nlwYNf8lDtBBZRuMJtik_A5tx3xidatdWKKPtIPCapmg-TCgRFphAqIZoSaPa1TTwhaBmXgds_K7KFENmNgu8OzFurZaaXXQrGakbnjlqWBNA=s2048">
</p>






<h4>  5.6 HUD</h4> 

<h4>  5.7 Camera</h4> 

In INDIVIDUATIO, the game utilizes a single 3rd person camera perspective. The camera follows
the hero and provides a view of the surroundings and enemies, allowing the player to strategically
plan their movements and attacks. The camera also has a slight shake effect to enhance the intense
moments in the game. The shake effect is synchronized with the actions of the player, such as when


the hero is engaged in combat with enemies. This camera design enhances the immersive experience
for the player, as they feel more connected to the hero and the action in the game.

<h3>  6 Game World</h3>

Our game revolves around a big forest type of maze. From the player perspective, the maze is supposed
to look like its never ending. We implemented an infinite number of large trees and types of green
in the playable area, that gives the player, the feeling of being lost and ”deja vu” like that path has
already been made.
The forest maze is divided into several parts, each with a certain colour of the mask associated.


