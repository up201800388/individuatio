using UnityEngine;

public class FollowAndSpin : MonoBehaviour
{
    public Transform target; // The target GameObject to follow
    public float followDistance = 10f; // The distance to keep from the target
    public float followSpeed = 5f; // The speed at which to follow the target
    public float spinSpeed = 180f; // The speed at which to spin around the target

    void Update()
    {
        // Move towards the target
        //transform.position = Vector3.Lerp(transform.position, target.position, followSpeed * Time.deltaTime);

        // Keep the desired distance from the target
        //transform.position = transform.position + (transform.position - target.position).normalized * followDistance;

        // Spin around the target
        transform.RotateAround(target.position, Vector3.up, spinSpeed * Time.deltaTime);
    }
}
