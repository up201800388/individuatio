using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip callAtention;
    private AudioSource audioSource;
    private Animator animator;
    private bool speaking = false;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Cast();
    }

    public void Cast()
    {
        int layerMask = 1 << 9;


        RaycastHit hit;
        Vector3 p1 = transform.position;
        if (Physics.SphereCast(p1, 4, transform.forward, out hit, 10, layerMask))
        {
            if (!speaking)
            {
                speaking = true;
                Sequence speak = DOTween.Sequence()
                        .AppendCallback(() => audioSource.clip = callAtention)
                    .AppendCallback(() =>
                    {
                        Dialogue d = GameObject.Find("DialogueBox").GetComponent<Dialogue>();
                        d.lines = new string[] {

                        "Welcome to the dream world, young adventurer.",
                        "This is a place where you can collect special masks that give you unique powers and abilities.",
                        "The masks are highlighted by different colors: Green, Blue, and Red.",
                        "But be warned, you must collect the masks in a specific order: Green -> Blue -> Red.",
                        "Otherwise, you will not be able to gather the other masks.",
                        "As you explore the dream world, you will encounter various enemies.",
                        "These enemies can only be defeated by hitting them with the same color as your mask.",
                        "You can swap between the collected masks at any time, use your strategy and quick thinking.",
                        "Collect all the masks and overcome the challenges of the dream world.",
                        "Good luck on your journey, brave adventurer."


                        };
                        d.EventToHappen = 1;
                        animator.SetBool("Speaking", speaking);
                        audioSource.Play();
                    }).Insert(0, DOTween.To(() => 0, x =>
                    {

                    }, 0, 4)).AppendCallback(() =>
                    {

                    }).Insert(0, DOTween.To(() => 0, x =>
                    {

                    }, 0, 4)).AppendCallback(() =>
                    {


                    });
            }

        }
    }
}
