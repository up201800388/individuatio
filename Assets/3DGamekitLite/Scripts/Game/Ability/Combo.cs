using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamekit3D;
using UnityEngine.Rendering;
using DG.Tweening;
using Cinemachine;

public class Combo : AbilityBase
{
    [Header("Connections")]
    [SerializeField] private Animator animator = default;
    [SerializeField] private CinemachineFreeLook originalCam = default;
    [SerializeField] private MeleeWeapon weapon = default;
    [SerializeField] private Damageable damageable = default;
    [Header("Visuals")]
    [SerializeField] private Renderer skinnedMesh = default;
    public override void Ability()
    {
        animator.SetTrigger("Combo");

        Sequence dash = DOTween.Sequence()
        .AppendCallback(() => { damageable.isInvulnerable = true; weapon.gameObject.active = true; })
        .AppendCallback(() => SetWeaponHitbox(3))
        .Insert(0, transform.DOMove(transform.position + (transform.forward * 2), 3f))
        .Insert(0, skinnedMesh.material.DOFloat(1, "FresnelAmount", .1f))
        .Append(skinnedMesh.material.DOFloat(0, "FresnelAmount", .35f))
        .AppendCallback(() => SetWeaponHitbox(0.45f))
        .AppendCallback(() => { damageable.isInvulnerable = false; weapon.gameObject.active = false; });



        DOVirtual.Float(40, 50, .1f, SetCameraFOV)
            .OnComplete(() => DOVirtual.Float(50, 40, .5f, SetCameraFOV));
    }

   

    void SetCameraFOV(float fov)
    {
        originalCam.m_Lens.FieldOfView = fov;
    }
    void SetWeaponHitbox(float size)
    {
        foreach (var point in weapon.attackPoints)
            point.radius = size;
    }
}
