using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class Masks : MonoBehaviour
{
    [SerializeField] public GameObject mask1 = default;
    [SerializeField] public GameObject mask2 = default;
    [SerializeField] public GameObject mask3 = default;
    protected PlayerInput m_Input; // Reference used to determine how Ellen should move.
    private Renderer renderer;
    public float duration = 1f;
    public GameObject Sword = default;
    public string ActiveMask;
    private List<string> list = new List<string>();
    public GameObject dragon;
    bool canSwap = true;

    void Start()
    {
        m_Input = GetComponent<PlayerInput>();
        mask1 = GameObject.Find("GREEN");
        mask2 = GameObject.Find("RED");
        mask3 = GameObject.Find("BLUE");
        renderer = GetComponentInChildren<Renderer>();
        mask1.GetComponent<Renderer>().material.color = Color.black;
        mask2.GetComponent<Renderer>().material.color = Color.black;
        mask3.GetComponent<Renderer>().material.color = Color.black;
        mask1.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
        mask2.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
        mask3.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
        ActiveMask = "WHITE";
    }


    // Update is called once per frame
    void Update()
    {
        //  light.color = Color.Lerp(startColor, endColor, currentTime / duration);
        if (canSwap && m_Input.TabPress && m_Input.MoveInput == Vector2.zero)
        {
            Sword.GetComponent<Renderer>().material.SetFloat("_Emission", 2);

            canSwap = false;
            GameObject middle = GameObject.FindGameObjectWithTag("middle");
            GameObject left = GameObject.FindGameObjectWithTag("left");
            GameObject right = GameObject.FindGameObjectWithTag("right");

            Sequence reset = DOTween.Sequence().Insert(0, DOTween.To(() => 0, x => { }, 0, 0.5f))
                .AppendCallback(() => {
                    canSwap = true;
                });

            if (Mathf.Abs(mask1.transform.position.y - middle.transform.position.y) < 0.2f)
            {
                Color color = list.Contains("MASK GREEN") ? Color.green : Color.white;
                ActiveMask = list.Contains("MASK GREEN") ? "GREEN" : "WHITE";
                renderer.material.SetColor("_EmissionColor", color);
                mask1.GetComponent<Renderer>().material.color = color;
                mask1.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
                Sword.GetComponent<Renderer>().material.SetColor("_EmissionColor", color * 2);
                Sword.GetComponent<Renderer>().material.color = color;

                mask1.transform.DOMove(left.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask2.transform.DOMove(right.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask3.transform.DOMove(middle.transform.position, .1f)
                    .SetEase(Ease.InOutQuad);
            }
            else if (Mathf.Abs(mask1.transform.position.x - left.transform.position.x) < 0.2f)
            {
                Color color = list.Contains("MASK BLUE") ? Color.blue : Color.white;
                ActiveMask = list.Contains("MASK BLUE") ? "BLUE" : "WHITE";
                renderer.material.SetColor("_EmissionColor", color);
                mask2.GetComponent<Renderer>().material.color = color;
                mask2.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
                Sword.GetComponent<Renderer>().material.SetColor("_EmissionColor", color * 2);

                Sword.GetComponent<Renderer>().material.color = color;

                mask1.transform.DOMove(right.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask2.transform.DOMove(middle.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask3.transform.DOMove(left.transform.position, .1f)
                    .SetEase(Ease.InOutQuad);
                ;
            }
            else if (Mathf.Abs(mask1.transform.position.x - right.transform.position.x) < 0.2f)
            {
                Color color = list.Contains("MASK RED") ? Color.red : Color.white;
                ActiveMask = list.Contains("MASK RED") ? "RED" : "WHITE";

                renderer.material.SetColor("_EmissionColor", color);

                mask3.GetComponent<Renderer>().material.color = color;
                mask3.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
                Sword.GetComponent<Renderer>().material.SetColor("_EmissionColor", color * 2);

                Sword.GetComponent<Renderer>().material.color = color;

                mask1.transform.DOMove(middle.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask2.transform.DOMove(left.transform.position, .1f)
                    .SetEase(Ease.InOutQuad)
                    ;

                mask3.transform.DOMove(right.transform.position, .1f)
                    .SetEase(Ease.InOutQuad);
            }
        }

        if (!dragon.activeInHierarchy && list.Count == 3)
        {
            dragon.SetActive(true);
        }
    }

    public void setNewMask(string m)
    {
        list.Add(m);
    }

    internal bool CanIgetThis(string name)
    {
        switch (name)
        {
            case "MASK BLUE":
                return list.Contains("MASK GREEN");
                break;
            case "MASK RED":
                return list.Contains("MASK GREEN") && list.Contains("MASK BLUE");
                break;
            case "MASK GREEN":
                return true;
                break;
        }

        return false;
    }
}