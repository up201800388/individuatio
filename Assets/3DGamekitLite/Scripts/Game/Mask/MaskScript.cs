using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskScript : MonoBehaviour
{
    public float amplitude = .2f; // The amplitude of the bounce
    public float frequency = 3f; // The frequency of the bounce
    public float rotationSpeed = 1f; // The rotation speed (degrees per second)
    private Vector3 startPosition; // The starting position of the object
    public List<GameObject> CombatDirector; // The starting position of the object

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        // Calculate the new position based on the sine wave
        float y = amplitude * Mathf.Sin(frequency * Time.time);
        Vector3 newPosition = new Vector3(transform.position.x, startPosition.y + y, transform.position.z);

        // Update the position of the object
        transform.position = newPosition;

        // Rotate the object around its own axis
       transform.RotateAround(Vector3.up, rotationSpeed * Time.deltaTime);
     
    }
    
    public void Picked() {
        foreach (var combatDirector in CombatDirector)
        {
            var director = Instantiate(combatDirector, this.gameObject.transform.position, Quaternion.identity);
            director.SetActive(true);
        }
            
        Destroy(this.gameObject, 0f);
    }
}
