using System;
using DG.Tweening;
using Gamekit3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Dragon : MonoBehaviour
{
    private Animator m_animator;
    private bool inBattle = false;
    public AudioClip roar;
    private AudioSource m_audio;
    private GameObject m_target;
    private float speed=0;
    private bool attacking = false;
    public int healthPoints = 1;
    private bool canReceiveDMG;

    private Slider healthBar;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponentInChildren<Slider>();
        m_animator = GetComponent<Animator>();
        m_audio = GetComponent<AudioSource>();
        canReceiveDMG = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!inBattle)
            Cast();

        if (m_target != null)
        {
            Move(speed);
            if (!attacking)
                transform.LookAt(m_target.transform);
            float distance = Vector3.Distance(m_target.transform.position,transform.position);
            if (distance <= 10.0f && distance >= 5.0f)
            {
                m_animator.SetFloat("Speed", 0.2f);
              
            }
            else if (distance > 10.0f)
            {
                m_animator.SetFloat("Speed", 1f);
                
            }
            else {

                string[] attkcs = { "CAttack", "BAttack", "HAttack" };
                m_animator.SetFloat("Speed", 0f);
                if (!attacking)
                {
                    Debug.Log("Attk");

                    attacking = true;
                    m_animator.SetTrigger(attkcs[UnityEngine.Random.Range(0, 3)]);
                }
              
                
            }
        }
    }

    public void Cast()
    {
        int layerMask = 1 << 9;


        RaycastHit[] hits;
        Vector3 p1 = transform.position;
        hits = Physics.SphereCastAll(p1, 15, transform.TransformDirection(Vector3.forward), 2f, layerMask);
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.gameObject.name.Equals("Ellen")){
                Debug.Log("Starting");
                m_audio.clip = roar;
                m_audio.Play();
                m_animator.SetTrigger("Scream");
                m_target = hit.collider.gameObject;
                inBattle = true;
            }

        }
    }
    
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 3);
    }
    
    void Move(float d)
    {
        if(!attacking)
            transform.position += transform.forward * d * Time.deltaTime;
    }
    void setSpeed(float s)
    {
        speed = s;
    }
    void setAtackingTrue()
    {
        attacking = true;
    }
    public void DoDmg() {

        int layerMask = 1 << 9;
        RaycastHit[] hits;
        Vector3 p1 = transform.position;
        hits = Physics.SphereCastAll(p1, 4, transform.TransformDirection(Vector3.forward), 0.5f, layerMask);
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.gameObject.name.Equals("Ellen"))
            {
                Damageable.DamageMessage damageMessage = new Damageable.DamageMessage();
                damageMessage.damageSource = transform.position;
                damageMessage.amount = 1;
                damageMessage.damager = this; 
                hit.collider.gameObject.GetComponent<Damageable>().ApplyDamage(damageMessage);
            }

        }
    }
    void setAtackingFalse()
    {
        Debug.Log("setAtackingFalse");

        attacking = false;
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (canReceiveDMG && other.name.Equals("MagicSword_Iron"))
        {
            if (healthPoints > 1)
            {
                canReceiveDMG = false;
                StartCoroutine(ResetDMG());
                m_animator.SetTrigger("GetHit");
                healthPoints--;
                healthBar.value = healthPoints;
            }
            else
            {
                m_animator.Play("dieN");
                healthBar.gameObject.SetActive(false);
            }

        }
    }
    IEnumerator ResetDMG()
    {
        yield return new WaitForSeconds(2f);
        canReceiveDMG = true;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 p1 = transform.position ;


        Gizmos.DrawWireSphere(p1, 15);
    }
}
